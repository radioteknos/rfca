#!/usr/bin/env python3

# 
#   rfca - RF cascade analyzer a simple tool to evaluate NF, G, P1dB,
#          IP3 of radiofrequency cascaded systems
# 
#      (c) by Lapo Pieri 2020
# 
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
# 
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
# 
#  Send bugs reports, comments, critique, etc, to
# 
#        lapo_CHANGE_THIS_AT_CHANGE_THIS_radioteknos.it
# 

import os, sys, getopt, configparser
import re, decimal
from math import pow, log10, sqrt

ansi_color={
    "ANSI_RESET"      :"\033[0:00m",
    "ANSI_BOLD"       :"\033[0:01m",
    "ANSI_NORMAL"     :"\033[0:22m",
    "ANSI_ITALIC"     :"\033[0:03m",
    "ANSI_UNDERLINE"  :"\033[0:04m",
    "ANSI_BLINK"      :"\033[0:05m",
    "ANSI_REVERSE"    :"\033[0:07m",
    "ANSI_FG_BLACK"   :"\033[0:30m",
    "ANSI_FG_RED"     :"\033[0:31m",
    "ANSI_FG_GREEN"   :"\033[0:32m",
    "ANSI_FG_YELLOW"  :"\033[0:33m",
    "ANSI_FG_BLUE"    :"\033[0:34m",
    "ANSI_FG_MAGENTA" :"\033[0:35m",
    "ANSI_FG_CYAN"    :"\033[0:36m",
    "ANSI_FG_WHITE"   :"\033[0:37m",
    "ANSI_BG_BLACK"   :"\033[0:40m",
    "ANSI_BG_RED"     :"\033[0:41m",
    "ANSI_BG_GREEN"   :"\033[0:42m",
    "ANSI_BG_YELLOW"  :"\033[0:43m",
    "ANSI_BG_BLUE"    :"\033[0:44m",
    "ANSI_BG_MAGENTA" :"\033[0:45m",
    "ANSI_BG_CYAN"    :"\033[0:46m",
    "ANSI_BG_WHITE"   :"\033[0:47m",
}


def main():

    run_template=False
    verbose=0
    stages=0    
    fill_nf=1
    
    # command line params scan
    try:
        opts, args = getopt.gnu_getopt(sys.argv[1:], 'hsvn',
                                       ['help',
                                        'template',
                                        'stages',
                                        'verbose',
                                        'only-numbers',
                                        'no-fill-nf'
                                       ])        
    except getopt.GetoptError:
        print("unknown switch", sys.argv[1:])        
        use()
        sys.exit(1)

    for opt, arg in opts:
        if opt in ("-h", "--help"):
            use()
            sys.exit(0)
            
        elif opt in ("--template"):
            run_template=True
            
        elif opt in ("-v", "--verbose"):
            verbose=1

        elif opt in ("-s", "--stages"):
            stages=1
            
        elif opt in ("-n", "--only-numbers"):
            onlynum=1
            quiet=1
        
        elif opt in ("--no-fill-nf"):
            fill_nf=0
            
    if(run_template==True):
        template()
        sys.exit(0)

    if(verbose==1):
        print(ansi_color["ANSI_FG_BLUE"]+ansi_color["ANSI_BOLD"]+\
            "\n                      RF Cascade Analisys Tool \n"+\
            ansi_color["ANSI_RESET"])
        
    if(len(args)==0):
        use()
        sys.exit(0)


        
    # open source file as config file
    srcfilename=args[0]
    srcbasename=os.path.splitext(srcfilename)
    # exit if no source file is given
    if(os.path.isfile(srcfilename)==False):
        use()
        sys.exit(1)        
    src=configparser.ConfigParser()
    try:
        src.read(srcfilename)
    except configparser.ParsingError as err:
        print(err)
        sys.exit(1)        

    print("# block   G        NF    IIP2     OIP2    IIP3    OIP3")
    print("# ------------------------------------------------------")

    # var init
    stage=0
    gtot=0.
    ftot=1.
    iip2tot=0.
    iip3tot=0.
    nf_analisys=1
    ip2_analisys=0
    ip3_analisys=0
    
    # scan for every block in source file and store in dict
    chain={}
    for block in src.sections():
        # parama=[g, nf, iip2, oip2, iip3, oip3, P1dB]
        # param[0]=G
        # param[1]=NF
        # param[2]=IIP2
        # param[3]=OIP2
        # param[4]=IIP3
        # param[5]=OIP3
        # param[6]=P1dB
        params=['', '', '', '', '', '', '']
        for par in src.items(block):

            val=umprefdec(par[1])
            v=val[0]*val[1]
            if(par[0].upper()=="G"):
                params[0]=v
            elif(par[0].upper()=="NF"):
                params[1]=v
            elif(par[0].upper()=="IIP2"):
                params[2]=v
            elif(par[0].upper()=="OIP2"):
                params[3]=v
            elif(par[0].upper()=="IIP3"):
                params[4]=v
            elif(par[0].upper()=="OIP3"):
                params[5]=v
            elif(par[0].upper()=="P1DB"):
                params[6]=v

        chain[block]=params

    # error checking
    for blk in chain:
        # with missing gain no analisys is possible
        if(chain[blk][0]==''):
            print("missed gain for block: %s" % blk)
            sys.exit(1)

        # it's ok to not have NF but analisys won't be performed
        if(chain[blk][1]==''):
            if(fill_nf==0):
                nf_analisys=0
            else:
                if(chain[blk][0]<0.):
                    chain[blk][1]=-chain[blk][0]
                else:
                    nf_analisys=0
        
        # if only oip2 or iip2 are given fill chain{} with other
        if(chain[blk][2]=='' and chain[blk][3]!=''):
            chain[blk][2]=chain[blk][3]-chain[blk][0]
            ip2_analisys=1
        elif(chain[blk][2]!='' and chain[blk][3]==''):
            chain[blk][3]=chain[blk][2]+chain[blk][0]
            ip2_analisys=1
        # if oipN and iipN are given the must match with gain
        elif(chain[blk][2]!='' and chain[blk][3]!=''):
            ip2_analisys=1
            if(abs(chain[blk][3]-chain[blk][2]-chain[blk][0])>0.5):
                print("Incoernt data for block %s:\
G=%7.2fdB, IIP2=%7.2fdBm, OIP2=%7.2fdBm" %
                      (blk, chain[blk][0], chain[blk][2], chain[blk][3]))
                sys.exit(1)

        # if only oip3 or iip3 are given fill chain{} with other
        if(chain[blk][4]=='' and chain[blk][5]!=''):
            chain[blk][4]=chain[blk][5]-chain[blk][0]
            ip3_analisys=1
        elif(chain[blk][4]!='' and chain[blk][5]==''):
            chain[blk][5]=chain[blk][4]+chain[blk][0]
            ip3_analisys=1
        # if oipN and iipN are given the must match with gain
        elif(chain[blk][4]!='' and chain[blk][5]!=''):
            ip3_analisys=1
            if(abs(chain[blk][5]-chain[blk][4]-chain[blk][0])>0.5):
                print("Incoernt data for block %s:\
G=%7.2fdB, IIP3=%7.2fdBm, OIP3=%7.2fdBm" %
                      (blk, chain[blk][0], chain[blk][4], chain[blk][5]))
                sys.exit(1)


    if(ip2_analisys==1):
        for blk in chain:
            if(chain[blk][2]==''):
                chain[blk][2]=+1000.
                chain[blk][3]=+1000.
    if(ip3_analisys==1):
        for blk in chain:
            if(chain[blk][4]==''):
                chain[blk][4]=+1000.
                chain[blk][5]=+1000.
        
                
    # calc G, NF, IP2 and IP3
    for blk in chain:
        # NF
        if(nf_analisys!=0):
            f=10**(chain[blk][1]/10.)
            fn=(f-1.)/(10.**(gtot/10.))
            ftot+=fn

        # IP2
        if(ip2_analisys!=0):
            iip2tot+=pow(10, (gtot-chain[blk][2])/20.)

            
        # IP3
        if(ip3_analisys!=0):
            iip3tot+=pow(10, (gtot-chain[blk][4])/10.)

        # G
        # to reduce calcs gtot has to be evaluated at the end
        gtot+=chain[blk][0]        

        if(stages==1):
            print(blk, "\t%7.2f" % gtot, end='')

            if(nf_analisys!=0):
                print("\t%7.2f" % (10*log10(ftot)), end='')
            else:
                print("\t", end='')
            
            if(ip2_analisys!=0):
                iip2dB=-20.*log10(iip2tot)
                if(iip2dB<200.):
                    print("%7.2f" % (iip2dB), end='\t')
                    print("%7.2f" % (iip2dB+gtot), end='')
                else:
                    print("\t\t\t", end='')
            else:
                print("\t\t\t", end='')

            if(ip3_analisys!=0):
                iip3dB=-10.*log10(iip3tot)
                if(iip3dB<200.):
                    print("%7.2f" % (iip3dB), end='\t')
                    print("%7.2f" % (iip3dB+gtot))
                else:
                    print("\n", end='')
            else:
                print("\n", end='')
        
    # print total chain values
    if(stages==0):
        print("TOT:\t%7.2f" % gtot, end='')
        
        if(nf_analisys!=0):
            print("\t%7.2f" % (10*log10(ftot)), end='')
        
        if(ip2_analisys!=0):
            print("%7.2f" % (-20.*log10(iip2tot)), end='\t')
            print("%7.2f" % ((-20.*log10(iip2tot))+gtot), end='')

        if(ip3_analisys!=0):
            print("%7.2f" % (-10.*log10(iip3tot)), end='\t')
            print("%7.2f" % ((-10.*log10(iip3tot))+gtot))
        
# main end       

def umprefdec(val):
    prefs=[['dBm', 1], ['dB', 1], ['f', 1e-15], ['p', 1e-12], ['n', 1e-9],
           ['u', 1e-6], ['m', 1e-3], ['k', 1e3], ['M', 1e6], ['G', 1e9],
           ['T', 1e12]]

    val=val.replace(" ", "")
    val=val.replace("\t", "")
    has_pref=False
    for p in prefs:
        if p[0] in val:
            m=p[1]
            has_pref=True
            break

    if has_pref:
        pos=val.find(p[0])
        v=float(val[:(pos)])
        um=val.strip()[pos+1:]
    else:
        m=1
        numeric_const_pattern='[-+]? (?: (?: \d* \. \d+ ) | (?: \d+ \.? ) )(?: [Ee] [+-]? \d+ ) ?'
        rx=re.compile(numeric_const_pattern, re.VERBOSE)
        v=rx.findall(val)[0]
        um=val[(len(v)):]
        v=float(v)
        
    return (v, m, um)

# how to use
def use():
    print("usage:")
    print("    $ rfca [options] <filename>.cas")
    print("  -h | --help : (this) help")
    print("  -s | --stagese : show stage-by-stege values")
    print("  -v | verbose : show fields. not just numbers")
    print("  -n | --only-numbers : minimal printout")
    print("  --template : print a simple template (hint: redirect on a .cas file)")
    print("  --no-fill-nf : disable rule: if NF is not specified and G<0.dB than NF=-G")
    ''
    print("Notes:")
    print(" - for not given IP2/IP3 values +100dBm is assumed")
    print(" - P1dB require more accurate analisys using real source power")
    print(" - calculating P1db as someting \"~10dB under IP3\" is not usefull")
    print("")
    
# print a template
def template():
    print("""
# rf cascade analisys template
#
# recognized params:
# g: gain (specify kind of gain)
# nf: noise figure relative to T=270K
# P1dB: 1dB compression point
# iip3: input refered 3rd point intercept
# oip3: output refered 3rd point intercept
# iip2: input refered 2nd point intercept
# oip2: output refered 2nd point intercept

[AMP1]
nf=1.4dB
g=+13dB
iip2=+19dBm
#oip2=+32dBm
iip3=+4dBm

[FLT1]
g=-2dB
nf=2dB

[FLT2]
g=-3dB
nf=3dB

[AMP2]
g=+24dB
nf=3.5dB
iip3=+20dBm
iip2=+27dBm

# Gtot=32dB
# NFtot= 2.27dB
    """)

if __name__ == '__main__':
    main()
