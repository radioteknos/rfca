## rfca - RF Cascade Analisys Tool
A simple radiofrequency cascade analisys tool that can calculate G,
NF, IP2 and IP3 in typical RF systems

## quick start

As a first, try:

    $ rfca -h

                      RF Cascade Analisys Tool 

usage:
    $ rfca [options] \<filename\>.cas

-h | --help : (this) help

-q | --quiet : reduce printout

  -v | verbose : show stage-by-stege values

  -n | --only-numbers : minimal printout

  --template : print a simple template (hint: redirect on a .cas file)
  
Notes:

 - for not given IP2/IP3 values +100dBm is assumed
 
 - P1dB require more accurate analisys using real source power
 
 - calculating P1db as someting "~10dB under IP3" is not usefull


and then:

    $ rfca --template >template.cas
	
take a look at template.cas and then try a real simulation with:

    $ rfca template.cas

to get:

                      RF Cascade Analisys Tool 

Gtot=   32.00

NFtot=    2.27

IIP2=   12.98	OIP2=   44.98

IIP3=    3.36	OIP3=   35.36
